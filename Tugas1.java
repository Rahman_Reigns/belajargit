import java.util.Scanner;

public class Tugas1 {
  public static void main(String[] args) {
    String nama = "Wahyudi Ilahi Rahman";
    String alamat = "Pringgabaya";
    int angka1;
    int angka2;
    int hasil = 0;
    int pilihan;
    Scanner masukkan_angka = new Scanner(System.in);
    System.out.println("TUGAS JAVA PEKAN 1");
    System.out.println("1.Identitas");
    System.out.println("2.Kalkulator");
    System.out.println("3.Perbandingan");

    System.out.println("=======================");
    System.out.print("Masukkan pilihan anda : ");
    pilihan = masukkan_angka.nextInt();
    if (pilihan == 1) {
      System.out.println("Anda memilih menu identitas");
      System.out.println("Nama saya adalah " + nama);
      System.out.println("Alamat saya adalah " + alamat);
    } else if (pilihan == 2) {
      System.out.println("Anda memilih menu kalkulator");
      System.out.println("1.PERKALIAN");
      System.out.println("2.PENJUMLAHAN");
      System.out.println("3.PEMBAGIAN");
      System.out.println("4.PENGURANGAN");
      System.out.println("Bilangan 1 : ");
      angka1 = masukkan_angka.nextInt();

      System.out.println("Bilangan 2 : ");
      angka2 = masukkan_angka.nextInt();
      System.out.println("Pilih operasia aritmatika : ");
      pilihan = masukkan_angka.nextInt();
      switch (pilihan) {
        case 1:
          hasil = angka1 * angka2;
          break;
        case 2:
          hasil = angka1 + angka2;
          break;
        case 3:
          hasil = angka1 - angka2;
          break;
        case 4:
          hasil = angka1 % angka2;
          break;
        default:
          System.out.println("salah memasukkan pilahan ");

      }
      System.out.println(" Hasil : " + hasil);

    } else if (pilihan == 3) {
      System.out.println("Anda memilih menu perbandingan");
      System.out.println("Bilangan 1 : ");
      angka1 = masukkan_angka.nextInt();
      System.out.println("Bilangan 2 : ");
      angka2 = masukkan_angka.nextInt();

      if (angka1 > angka2) {
        System.out.println(angka1 + ">" + angka2);
      } else if (angka1 < angka2) {
        System.out.println(angka1 + " < " + angka2);
      } else if (angka1 == angka2) {
        System.out.println(angka1 + "==" + angka2);
      } else {
        System.out.println("nokomen");
      }

    }
  }

}
